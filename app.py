from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:password@localhost/spotify_db'
db = SQLAlchemy(app)
app.debug = True
migrate = Migrate(app, db)

class Artist(db.Model):
    __tablename__ = 'artist'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    location = db.Column(db.String(), nullable=False)
    likes = db.Column(db.Integer(), nullable=False, default=0)

    def __init__(self, name, location, likes):
        self.name = name
        self.location = location
        self.likes = likes
    
@app.route('/test', methods=['GET'])
def test():
    return{
        'test': 'test1'
    }

@app.route('/artist', methods=['GET'])
def get_artist():
    allArtists = Artist.query.all()
    output = []
    for artist in allArtists:
        currArtist = {}
        currArtist['name'] = artist.name
        currArtist['location'] = artist.location
        currArtist['likes'] = artist.likes
        output.append(currArtist)
    return jsonify(output)

@app.route('/artist/<artist_id>', methods=['GET'])
def search_artist(artist_id):
    artist = Artist.query.get_or_404(artist_id)
    response = {
        "name": artist.name,
        "location": artist.location,
        "likes": artist.likes
    }
    return jsonify(response)


@app.route('/artist', methods=['POST'])
def post_artist():
    artistDetail = request.get_json()
    artist = Artist(name=artistDetail['name'], location=artistDetail['location'], likes=artistDetail['likes'])
    db.session.add(artist)
    db.session.commit()
    return jsonify(artistDetail)

@app.route('/artist/update/<artist_id>', methods=['PUT'])
def update_artist(artist_id):
    artistDetail = request.get_json()
    artist = Artist.query.get_or_404(artist_id)
    artist.name = artistDetail['name']
    artist.location = artistDetail['location']
    artist.likes = artistDetail['likes']
    db.session.add(artist)
    db.session.commit()
    return jsonify(artistDetail)

@app.route('/artist/delete/<artist_id>', methods=['DELETE'])
def delete_artist(artist_id):
    artist = Artist.query.get_or_404(artist_id)
    db.session.delete(artist)
    db.session.commit()
    return {"message": f"Artist {artist.name} successfully deleted."}


if __name__ == '__main__':
    app.run(debug=True)